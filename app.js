require('./config/config');
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const authorization = require('./route/authorization')
const person = require('./route/person')
const factory = require('./route/factory')
const participant = require('./route/participant')
const port = process.env.PORT

const app = express()
app.use(bodyParser.json())
app.use(cors())

app.use('/v1/authorization/', authorization)
app.use('/v1/person/', person)
app.use('/v1/factory/', factory)
app.use('/v1/participant/', participant)

app.listen(port, () => {
  console.log(`Express server listening on port: ${port}`)
})

module.exports = {app}