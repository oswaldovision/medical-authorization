module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*" // Match any network id
    },
    private: {
      host: "127.0.0.1",
      port: 8095,
      network_id: 13874025,// Match any network id
      gas: 1081238
    }
  }
};

// module.exports = {
//   networks: {
//     "live": {
//       network_id: 1,
//       host: "localhost",
//       port: 8095   // Different than the default below
//     }
//   },
//   rpc: {
//     host: "localhost",
//     port: 8545
//   }
// };