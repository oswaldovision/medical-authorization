var express = require('express')
var router = express.Router()
var Web3 = require('web3')

var web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8095'))


var factoryContact = web3.eth.contract([{"constant":false,"inputs":[{"name":"name","type":"string"},{"name":"doc","type":"string"}],"name":"addPerson","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"name","type":"string"},{"name":"nit","type":"string"},{"name":"phones","type":"string"}],"name":"addEps","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"code","type":"string"},{"name":"request","type":"string"},{"name":"doctor","type":"string"},{"name":"status","type":"string"}],"name":"addProcess","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"}])
var authorizationContract = web3.eth.contract([{"constant":false,"inputs":[{"name":"index","type":"string"},{"name":"personAddress","type":"string"},{"name":"epsAddress","type":"string"},{"name":"processAddress","type":"string"}],"name":"asignation","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"index","type":"string"}],"name":"getAuth","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"type":"function"}])
var personContract = web3.eth.contract([{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"document","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"inputs":[{"name":"namePerson","type":"string"},{"name":"doc","type":"string"}],"payable":false,"type":"constructor"}])
var epsContract = web3.eth.contract([{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"nit","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"phones","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"inputs":[{"name":"nameEps","type":"string"},{"name":"nitEps","type":"string"},{"name":"phonesEps","type":"string"}],"payable":false,"type":"constructor"}])
var processContract = web3.eth.contract([{"constant":true,"inputs":[],"name":"status","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"code","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"request","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"doctor","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"inputs":[{"name":"codeP","type":"string"},{"name":"requestP","type":"string"},{"name":"doctorP","type":"string"},{"name":"statusP","type":"string"}],"payable":false,"type":"constructor"}])

let Factory = factoryContact.at("0xfd7bb3903ab3a3589d925c1afb4cbe9abbc4babc")
let addressPerson = "";//personContract.at("0x4d29286c4a9ce41a0280c84252db74ad613b88fa")
// var Eps = epsContract.at("0x176991701f95cf3440b37047508e767b0b428da0")
// var Process = processContract.at("0x0558235dee1dd86cd57d57d574cab6aacd7be71e")
// var Authorization = authorizationContract.at("0x4862468a609e3d169d3dd5eeae3950eeb403ec49")

router.post('/addPerson', (req, res) => {
  var fac = factoryContact.new({from : web3.eth.accounts[0]})

  var newPerson = fac.addPerson.call(req.body.name, req.body.doc,{ from: web3.eth.accounts[0] })
  res.send(newPerson)
})

router.post('/getPerson', (req, res) => {
  let per = personContract.at(addressPerson)

  res.send(per.name())
})

module.exports = router