// require('./../config/config')
// require('./../db/mongoose')

const express = require('express')
const router = express.Router()
let {Participant} = require('./../schema/participant')

const {authenticate} = require('./../middleware/authenticate')

router.post('/addParticipant', authenticate,(req, res) => {

  let newParticipant = toParticipant(req.body)
  newParticipant.save().then(participant => {
    res.status(201).send(participant)
  }).catch(e => {
    res.status(400).send(e)
  })

})

let toParticipant = body => {
  return new Participant({
    'name': body.name,
    'lastName': body.lastName,
    'secondLastName': body.secondLastName,
    'numberDocument': body.numberDocument,
    'typeDocument': body.typeDocument,
    'birthday': body.birthday,
    'address': body.address,
    'phone': body.phone,
    'celPhone': body.celPhone,
    'email': body.email
  })
}


module.exports = router