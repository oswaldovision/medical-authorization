var express = require('express')
var router = express.Router()
var Web3 = require('web3')

var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8095'))

var factoryContract = web3.eth.contract([{"constant":false,"inputs":[{"name":"code","type":"string"},{"name":"person","type":"string"},{"name":"doctor","type":"string"},{"name":"ips","type":"string"},{"name":"status","type":"string"}],"name":"addAuthorization","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"name","type":"string"},{"name":"doc","type":"string"}],"name":"addPerson","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"name","type":"string"},{"name":"nit","type":"string"},{"name":"phones","type":"string"}],"name":"addIps","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"}]);
var factory = factoryContract.at("0x49519757a481c250a195d97aa48a239b6e1668c3")

router.post('/addPerson', (req, res) => {

  var person = factory.addPerson.call(req.body.name, req.body.document, {from : web3.eth.accounts[0]}, (err,result) => {
    if (!err){
      res.send(result)
    }
  })

  // Person.new(req.body.name,req.body.document ,{from : web3.eth.accounts[0], gas: 1000000}).then(instance=>{
  //   res.send(instance.address)
  // }).catch(err => {
  //   res.send(err.message)
  // })
})

module.exports = router