pragma solidity ^0.4.8;


contract Person {
    string public name;
    string public document;

    function Person(string namePerson , string doc){
        name = namePerson;
        document = doc;
    }
}


contract Ips{
    string public name;
    string public nit;
    string public phones;

    function Ips(string nameIps, string nitIps,string phonesIps){
        name = nameIps;
        nit = nitIps;
        phones = phonesIps;
    }
}

contract Authorization{
    string public codeA;
    string public personA;
    string public doctorA;
    string public ipsA;
    string public statusA;

    function Authorization(string code,string person,string doctor, string ips,string status ){
        codeA = code;
        personA = person;
        doctorA = doctor;
        ipsA = ips;
        statusA = status;
    }
}

contract Factory{
    function addPerson(string name, string doc) constant returns (address){
        return new Person(name ,doc);
    }

    function addIps(string name, string nit, string phones) constant returns (address){
        return new Ips(name, nit, phones);
    }

    function addAuthorization(string code, string person, string doctor, string ips, string status) constant returns (address){
        return new Authorization(code , person, doctor, ips,status);
    }
}

