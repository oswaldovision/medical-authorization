// var ConvertLib = artifacts.require("./ConvertLib.sol");
// var MetaCoin = artifacts.require("./MetaCoin.sol");
//
// module.exports = function(deployer) {
//   deployer.deploy(ConvertLib);
//   deployer.link(ConvertLib, MetaCoin);
//   deployer.deploy(MetaCoin);
// };

var Person = artifacts.require("./Person.sol");
var Ips = artifacts.require("./Ips.sol");
var Authorization = artifacts.require("./Authorization.sol");
var Factory = artifacts.require("./Factory.sol");


module.exports = function(deployer) {
  deployer.deploy(Person);
  deployer.deploy(Ips);
  deployer.deploy(Authorization);
  deployer.deploy(Factory);
}
