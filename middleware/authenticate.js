const jwt = require('jsonwebtoken');
var {User} = require('./../schema/user');

let authenticate = (req, res, next) =>{
  var token = req.header('x-auth');

  jwt.verify(token,process.env["JWT_SECRET"], (err, decoded) =>{
    if (err){
      return res.status(401).send({message : `${err.message} error` });
    }

    User.findByToken(token).then(user =>{

      if (!user){
        return Promise.reject();
      }
      req.user = user;
      req.token = token;
      next();
    }).catch(e =>{
      res.status(401).send({ message : `error  find user ${e}`});
    });
  });

};

module.exports = {authenticate};