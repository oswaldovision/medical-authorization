const {medicalConnection, mongoose} = require('./../db/mongoose')
const validator = require('validator')

const TYPEDOCS = ['cc', 'rc', 'ti', 'ce', 'pas', 'asi', 'msi']


let ParticipantSchema = new mongoose.Schema({
  name :{
    type: String,
    required: true,
    trim: true
  },
  lastName :{
    type: String,
    required: true,
    trim: true
  },
  secondLastName :{
    type: String,
    trim: true
  },
  numberDocument:{
    type: String,
    trim: true
  },
  typeDocument:{
    type: String,
    enum : TYPEDOCS
  },
  birthday:{
    type :Number,
    required: true,
    default : 1504618666
  },
  address:{
    type: String
  },
  phone:{
    type: String
  },
  celPhone:{
    type: String
  },
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: value => validator.isEmail(value),
      message: '{VALUE} is not a valid email'
    }
  }
})

let Participant = medicalConnection.model('Participant', ParticipantSchema, 'participants')

module.exports = {Participant}